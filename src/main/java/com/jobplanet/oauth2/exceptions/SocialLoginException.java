package com.jobplanet.oauth2.exceptions;

/**
 * 
 * @author Vemula Ruchitha
 *
 */
public class SocialLoginException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1910453119516184737L;

	/**
	 * 
	 */
	public SocialLoginException() {
		super("Unknown Social Login Exception Happened!!");
	}

	/**
	 * 
	 * @param msg
	 */
	public SocialLoginException(String msg) {
		super(msg);
	}

	/**
	 * 
	 * @param t
	 */
	public SocialLoginException(Throwable t) {
		super(t);
	}

	/**
	 * 
	 * @param msg
	 * @param t
	 */
	public SocialLoginException(String msg, Throwable t) {
		super(msg, t);
	}
}
