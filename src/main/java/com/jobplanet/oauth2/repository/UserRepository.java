package com.jobplanet.oauth2.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.jobplanet.oauth2.model.Role;
import com.jobplanet.oauth2.model.User;

/**
 * 
 * Licensed to the JobPlanet (JP) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership. The ASF licenses this file to you
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 * @author Ruchitha Vemula
 * @createdon 22-Jan-2020
 * @Package com.prutech.oauth2.repository
 * @Project oauth2-common
 *
 */
@Repository
public interface UserRepository extends JpaRepository<User, String> {
	//
	Optional<User> findByUsername(String username);

	//
	/*
	 * @Query("SELECT u from User u join u.roles r join r.authorities a WHERE a.authorityName=:authorityName and  r.organization_id=:orgId and "
	 * ) List<User> findUserByAuthorityName(@Param("authorityName") String
	 * authorityName, @Param("orgId") String orgId);
	 */
	//
	Optional<User> findByPhoneUserName(String userName);

	//
	Optional<User> findByphoneUserOtpAndOtpExpiryTimeGreaterThanEqual(String token, Date currentDate);

	//
	List<User> findByEnabledAndAccountLockedAndAccountExpiredAndRolesIn(boolean enabled, boolean accountLocked,
			boolean accountExpired, List<Role> roles);

}
