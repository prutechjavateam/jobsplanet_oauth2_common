package com.jobplanet.oauth2.utils;

import java.util.Arrays;
import java.util.Base64;

import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.simple.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.security.oauth2.client.token.grant.password.ResourceOwnerPasswordResourceDetails;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.web.client.RestTemplate;

import com.jobplanet.oauth2.constants.ClassConstants;
import com.jobplanet.oauth2.templates.Templates;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * Licensed to the JobPlanet (JP) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership. The ASF licenses this file to you
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 * @author Ruchitha Vemula
 * @createdon 22-Jan-2020
 * @Package com.prutech.oauth2.utils
 * @Project oauth2-common
 *
 */
@Slf4j
public class CommunicationUtil {

	public static String HEADERS_AUTHORIZATION_VALUE = "Authorization";
	//
	public static String HEADERS_AUTHORIZATION_BASIC_TYPE = "Basic ";
	//
	public static String HEADERS_AUTHORIZATION_BAREER_TYPE = "Bearer ";

	/**
	 * RestTemplate To Call OAuth End Point To Get OAuthToken For Social Tokens
	 * 
	 * @param details
	 * @param oauthTypeUrl
	 * @return
	 */
	public static OAuth2AccessToken callOauthTokenApiUsingRestTemplate(ResourceOwnerPasswordResourceDetails details,
			String oauthTypeUrl) {
		String method = new Throwable().getStackTrace()[0].getMethodName();
		RestTemplate template = new RestTemplate(getClientHttpRequestFactory());
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		String plainCreds = details.getClientId() + ClassConstants.Colon + details.getClientSecret();
		String base64Creds = Base64.getEncoder().encodeToString(plainCreds.getBytes());
		headers.add(HEADERS_AUTHORIZATION_VALUE, HEADERS_AUTHORIZATION_BASIC_TYPE + base64Creds);
		HttpEntity<String> entity = new HttpEntity<String>(ClassConstants.Parameters, headers);
		String tokenURL = details.getAccessTokenUri() + oauthTypeUrl;
		log.debug("{} {} {} {}", plainCreds, base64Creds, tokenURL, entity, ClassConstants.DoubleColon, method);
		OAuth2AccessToken responseEntity = template.postForObject(tokenURL, entity, OAuth2AccessToken.class);
		return responseEntity;
	}

	/**
	 * RestTemplate to call google Api for getting UserInfo
	 * 
	 * @param details
	 * @return
	 */
	public static ResponseEntity<JSONObject> callGoogleApiToGetUserInfoWithToken(String token) {
		String method = new Throwable().getStackTrace()[0].getMethodName();
		RestTemplate template = new RestTemplate(getClientHttpRequestFactory());
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.add(HEADERS_AUTHORIZATION_VALUE, HEADERS_AUTHORIZATION_BAREER_TYPE + token);
		HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
		String uri = "https://www.googleapis.com/oauth2/v1/userinfo?alt=json";
		log.debug("{} {} {}", token, uri, entity, ClassConstants.DoubleColon, method);
		ResponseEntity<JSONObject> responseEntity = template.exchange(uri, HttpMethod.GET, entity, JSONObject.class);
		return responseEntity;
	}

	/**
	 * 
	 * @return
	 */
	private static ClientHttpRequestFactory getClientHttpRequestFactory() {
		String method = new Throwable().getStackTrace()[0].getMethodName();
		int timeout = 50000;
		RequestConfig config = RequestConfig.custom().setConnectTimeout(timeout).setConnectionRequestTimeout(timeout)
				.setSocketTimeout(timeout).build();
		CloseableHttpClient client = HttpClientBuilder.create().setDefaultRequestConfig(config).build();
		log.debug("{}", timeout, ClassConstants.DoubleColon, method);
		return new HttpComponentsClientHttpRequestFactory(client);
	}

	/**
	 * 
	 * @param token
	 * @return
	 */
	public static ResponseEntity<JSONObject> callLinkedinApiToGetUserEmailWithToken(String token) {
		String method = new Throwable().getStackTrace()[0].getMethodName();
		RestTemplate template = new RestTemplate(getClientHttpRequestFactory());
		HttpHeaders headers = new HttpHeaders();
		headers.add(HEADERS_AUTHORIZATION_VALUE, HEADERS_AUTHORIZATION_BAREER_TYPE + token);
		HttpEntity<String> entity = new HttpEntity<String>(ClassConstants.Parameters, headers);
		String uri = "https://api.linkedin.com/v2/emailAddress?q=members&projection=(elements*(handle~))";
		log.debug("{} {} {} ", token, entity, uri, ClassConstants.DoubleColon, method);
		ResponseEntity<JSONObject> responseEntity = template.exchange(uri, HttpMethod.GET, entity, JSONObject.class);
		return responseEntity;
	}

	/**
	 * 
	 * @param token
	 * @return
	 */
	public static ResponseEntity<JSONObject> callLinkedinApiToGetUserIdWithToken(String token) {
		String method = new Throwable().getStackTrace()[0].getMethodName();
		RestTemplate template = new RestTemplate(getClientHttpRequestFactory());
		HttpHeaders headers = new HttpHeaders();
		headers.add(HEADERS_AUTHORIZATION_VALUE, HEADERS_AUTHORIZATION_BAREER_TYPE + token);
		HttpEntity<String> entity = new HttpEntity<String>(ClassConstants.Parameters, headers);
		String uri = "https://api.linkedin.com/v2/me";
		log.debug("{} {} {} ", token, entity, uri, ClassConstants.DoubleColon, method);
		ResponseEntity<JSONObject> responseEntity = template.exchange(uri, HttpMethod.GET, entity, JSONObject.class);
		return responseEntity;
	}

	/**
	 * 
	 * @param details
	 * @return
	 */
	public static String getOauthTypeUrlForOauthToken(ResourceOwnerPasswordResourceDetails details) {
		String method = new Throwable().getStackTrace()[0].getMethodName();
		String oauthTypeUrl = Templates.SOCIAL_OAUTH2_TOKEN_URL.replace("@@USERNAME@@", details.getUsername())
				.replace("@@PASSWORD@@", details.getPassword());
		log.debug("{} {}", details, oauthTypeUrl, ClassConstants.DoubleColon, method);
		return oauthTypeUrl;

	}

	/**
	 * 
	 * @param details
	 * @param refreshToken
	 * @return
	 */
	public static String getOauthTypeUrlForOauthRefreshToken(ResourceOwnerPasswordResourceDetails details,
			String refreshToken) {
		String method = new Throwable().getStackTrace()[0].getMethodName();
		String oauthTypeUrl = Templates.SOCIAL_OAUTH2_REFRESH_TOKEN_URL.replace("@@REFRESH_TOKEN@@", refreshToken);
		log.debug("{} {} {}", details, oauthTypeUrl, refreshToken, ClassConstants.DoubleColon, method);
		return oauthTypeUrl;
	}
}
