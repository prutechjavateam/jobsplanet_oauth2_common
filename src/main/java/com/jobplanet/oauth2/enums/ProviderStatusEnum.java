package com.jobplanet.oauth2.enums;

/**
 * 
 * Licensed to the JobPlanet (JP) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.

 * @author Ruchitha Vemula
 * @createdon 22-Jan-2020
 * @Package com.prutech.oauth2.enums
 * @Project oauth2-common
 *
 */
public enum ProviderStatusEnum {
	INACTIVE(0), ACTIVE(1);

	// statusCode
	private int statusCode;

	/**
	 * 
	 * @param statusCode
	 */
	private ProviderStatusEnum(int statusCode) {
		this.statusCode = statusCode;
	}

	public int getStatusCode() {
		return statusCode;
	}

	/**
	 * 
	 * @param name
	 * @return
	 */
	public static ProviderStatusEnum fromName(String name) {
		return valueOf(name.toUpperCase());
	}

	

}
