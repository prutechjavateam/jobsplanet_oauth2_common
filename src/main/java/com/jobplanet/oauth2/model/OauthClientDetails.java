package com.jobplanet.oauth2.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * 
 * Licensed to the Apache Software Foundation (ASF) under one
 * 
 * or more contributor license agreements. See the NOTICE file
 * 
 * distributed with this work for additional information
 * 
 * regarding copyright ownership. The ASF licenses this file
 * 
 * to you under the Apache License, Version 2.0 (the
 * 
 * "License"); you may not use this file except in compliance
 * 
 * with the License. You may obtain a copy of the License at
 *
 * 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * 
 * 
 * Unless required by applicable law or agreed to in writing,
 * 
 * software distributed under the License is distributed on an
 * 
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * 
 * KIND, either express or implied. See the License for the
 * 
 * specific language governing permissions and limitations
 * 
 * under the License.
 * 
 * @author Venkata Saikrishna Kodapaka
 * 
 * @createdon 12-Mar-2019
 * 
 * @Package com.prutech.oauth2.model
 * 
 * @Project oauth2-common
 *
 * 
 * 
 */
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class OauthClientDetails implements IGenericEntity {

	private static final long serialVersionUID = -7033397197456235048L;

	/*
	 * //
	 * 
	 * @Id
	 * 
	 * @GeneratedValue(generator = "system-uuid")
	 * 
	 * @GenericGenerator(name = "system-uuid", strategy = "uuid")
	 * 
	 * @Column(name="oauth_client_details_id") private String uuId;
	 */

	//
	@Id
	private String clientId;

	//
	private String resourceIds;

	//
	private String clientSecret;

	//
	private String scope;

	//
	private String authorizedGrantTypes;

	//
	private String webServerRedirectUri;

	//
	private String authorities;

	//
	private Integer accessTokenValidity;

	//
	private Integer refreshTokenValidity;

	//
	private String additionalInformation;

	//
	private String autoapprove;

	//
	private String otpExpiryTime;
	// To carry any kind of additional Data

	@Transient
	List<AdditionalData> additionalDataList = new ArrayList<AdditionalData>();
	//
	@Transient
	Response response;

	public void addAdditionData(AdditionalData additionalData) {
		getAdditionalDataList().add(additionalData);
	}

	public void setResponse(Response response) {
		this.response = response;
	}

}
