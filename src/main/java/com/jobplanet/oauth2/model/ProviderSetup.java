package com.jobplanet.oauth2.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Size;

import org.hibernate.annotations.GenericGenerator;

import com.jobplanet.oauth2.enums.ProviderStatusEnum;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * Licensed to the JobPlanet (JP) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership. The ASF licenses this file to you
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 * @author Ruchitha Vemula
 * @createdon 22-Jan-2020
 * @Package com.prutech.oauth2.model
 * @Project oauth2-common
 *
 */
@Entity
@Data
@NoArgsConstructor
public class ProviderSetup {
	/**
	 * 
	 */
	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	@Column(name = "provider_id")
	@Size(max = 36)
	private String attribId;
	//
	@Column
	String organizationId;
	//
	@Column
	String provider;
	//
	@Column(name = "attribute")
	String key;
	//
	@Column
	String value;
	//
	@Column
	String graphApiUrl;
	//
	@Column
	@Enumerated(EnumType.ORDINAL)
	ProviderStatusEnum status;

}
