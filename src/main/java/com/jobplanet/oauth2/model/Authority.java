package com.jobplanet.oauth2.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.security.core.GrantedAuthority;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * 
 * 
 * Licensed to the Apache Software Foundation (ASF) under one
 * 
 * or more contributor license agreements. See the NOTICE file
 * 
 * distributed with this work for additional information
 * 
 * regarding copyright ownership. The ASF licenses this file
 * 
 * to you under the Apache License, Version 2.0 (the
 * 
 * "License"); you may not use this file except in compliance
 * 
 * with the License. You may obtain a copy of the License at
 *
 * 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * 
 * 
 * Unless required by applicable law or agreed to in writing,
 * 
 * software distributed under the License is distributed on an
 * 
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * 
 * KIND, either express or implied. See the License for the
 * 
 * specific language governing permissions and limitations
 * 
 * under the License.
 * 
 * @author Venkata Saikrishna Kodapaka
 * 
 * @createdon 07-Mar-2019
 * 
 * @Package com.prutech.oauth2.model
 * 
 * @Project oauth2-common
 * 
 *
 *
 * 
 * 
 */
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@Table(uniqueConstraints = { @UniqueConstraint(columnNames = { "authorityName", "appModuleName" }) })
public class Authority extends BaseEntity implements GrantedAuthority, IGenericEntity {

	// serialVersionUID
	private static final long serialVersionUID = 4131102831847694688L;

	// authorityId
	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	@Column(name = "authority_id")
	private String authorityId;

	// authorityName
	@NotNull
	@Size(min = 1, max = 255, message = " you must use between 1 and 255 characters")
	private String authorityName;

	// application module name
	@NotNull
	@Size(min = 1, max = 255, message = " you must use between 1 and 255 characters")
	private String appModuleName;

	// application status
	@Column(columnDefinition = "TINYINT", nullable = false)
	private int status;

	public String getAuthority() {
		return getAuthorityName();
	}

	private Boolean isDefault;

	@Transient
	List<AdditionalData> additionalDatas = new ArrayList<AdditionalData>();
	//
	@Transient
	Response response;

	public void addAdditionData(AdditionalData additionalData) {
		additionalDatas.add(additionalData);
	}

	public void setResponse(Response response) {
		this.response = response;
	}

	//
	private Boolean isAdmin;
}
