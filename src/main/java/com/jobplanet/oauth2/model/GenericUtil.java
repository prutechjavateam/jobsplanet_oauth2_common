package com.jobplanet.oauth2.model;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

public class GenericUtil {

	/**
	 * 
	 * @param model
	 * @param fromMethodName
	 * @param opIndicator
	 * @return
	 */
	public static IGenericEntity rtnSucessObj(IGenericEntity model, String fromMethodName, ConstantsEnum opIndicator) {
		model.setResponse(new Response(String.valueOf(opIndicator.getCode()), successMessage(model, fromMethodName)));
		return model;
	}

	/**
	 * 
	 * @param model
	 * @param fromMethodName
	 * @param opIndicator
	 * @param errorArgs
	 * @return
	 */
	public static IGenericEntity rtnErrorObj(IGenericEntity model, String fromMethodName, ConstantsEnum opIndicator,
			Object... args) {
		model.setResponse(new Response(String.valueOf(opIndicator.getCode()),
				errorMessage(model, fromMethodName, opIndicator, args)));
		return model;
	}

	/**
	 * 
	 * @param model
	 * @param fromMethodName
	 * @param opIndicator
	 * @param args
	 * @return
	 */
	public static String errorMessage(IGenericEntity model, String fromMethodName, ConstantsEnum opIndicator,
			Object... args) {
		String returnMessage = opIndicator.getText().replaceAll(ConstantsEnum.OPERATION.getText(), fromMethodName)
				.replaceAll(ConstantsEnum.OBJECT.getText(), model.getClass().getSimpleName());
		for (Object iobj : args) {
			returnMessage = returnMessage.replaceFirst("(?s)@.*?@@", iobj.toString());
		}
		return returnMessage;
	}

	/**
	 * 
	 * @param obj
	 * @param fromMethodNameName
	 * @return
	 */
	public static String successMessage(Object obj, String fromMethodNameName) {
		String templateMessage = ConstantsEnum.OBJECT_OPERATION_SUCCESS.getText()
				.replaceAll(ConstantsEnum.OBJECT.getText(), obj.getClass().getSimpleName())
				.replaceAll(ConstantsEnum.OPERATION.getText(), fromMethodNameName);
		return templateMessage;
	}

	/**
	 * 
	 * @param obj
	 * @param template
	 * @param args
	 * @return
	 */
	public static String constructErrorMessage(Object obj, ConstantsEnum template, Object... args) {
		String templateMessage = template.getText().replaceAll(ConstantsEnum.OBJECT.getText(),
				obj.getClass().getSimpleName());
		for (Object iobj : args) {
			templateMessage = templateMessage.replaceFirst("(?s)@.*?@@", iobj.toString());
		}
		return templateMessage;
	}

	/**
	 * 
	 * @param obj
	 * @param template
	 * @param args
	 * @return
	 */
	public static String constructErrorMessage(Object obj, String template, Object... args) {
		String templateMessage = template.replaceAll(ConstantsEnum.OBJECT.getText(), obj.getClass().getSimpleName());
		for (Object iobj : args) {
			templateMessage = templateMessage.replaceFirst("(?s)@.*?@@", iobj.toString());
		}
		return templateMessage;
	}

	public static IGenericEntity objectSuccess(IGenericEntity model, String fromMethodName, ConstantsEnum opIndicator,
			Object... args) {
		model.setResponse(new Response(String.valueOf(opIndicator.getCode()),
				objectFound(model, fromMethodName, opIndicator, args)));
		return model;
	}

	public static String objectFound(IGenericEntity model, String fromMethodName, ConstantsEnum opIndicator,
			Object... args) {
		String templateMessage = opIndicator.getText().replaceAll(ConstantsEnum.OPERATION.getText(), fromMethodName)
				.replaceAll(ConstantsEnum.OBJECT.getText(), model.getClass().getSimpleName());
		for (Object iobj : args) {
			templateMessage = templateMessage.replaceFirst("(?s)@.*?@@", iobj.toString());
		}
		return templateMessage;
	}

	public static Pageable getPageable(int page, int rows) {
		return PageRequest.of(page, rows);
	}
}
