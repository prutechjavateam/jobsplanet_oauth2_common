package com.jobplanet.oauth2.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * 
 * 
 * Licensed to the Apache Software Foundation (ASF) under one
 * 
 * or more contributor license agreements. See the NOTICE file
 * 
 * distributed with this work for additional information
 * 
 * regarding copyright ownership. The ASF licenses this file
 * 
 * to you under the Apache License, Version 2.0 (the
 * 
 * "License"); you may not use this file except in compliance
 * 
 * with the License. You may obtain a copy of the License at
 *
 * 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * 
 * 
 * Unless required by applicable law or agreed to in writing,
 * 
 * software distributed under the License is distributed on an
 * 
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * 
 * KIND, either express or implied. See the License for the
 * 
 * specific language governing permissions and limitations
 * 
 * under the License.
 * 
 * @author Venkata Saikrishna Kodapaka
 * 
 * @createdon 07-Mar-2019
 * 
 * @Package com.prutech.oauth2.model
 * 
 * @Project oauth2-common
 *
 * 
 * 
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@MappedSuperclass
@SuperBuilder
public abstract class BaseEntity implements IGenericEntity, Serializable {

	private static final long serialVersionUID = -4700275768393452326L;

	// created name
	@Column
	private String createdBy;

	// creating Time and Date like This (yyyy-mm-dd hh:mm:ss)
	@Column(insertable = true, updatable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	@JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss z")
	private Date createdDate;

	// lastModifiedBy name
	@Column
	private String lastModifiedBy;

	// lastModified Date and Time like This(yyyy-mm-dd hh:mm:ss)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(insertable = true, updatable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP")
	@JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss z")
	private Date lastModifiedDate;

	/**
	 * Specifies the version field or property of an entity class that serves as its
	 * optimistic lock value. The version is used to ensure integrity when
	 * performing the merge operation and for optimistic concurrency control.
	 */
	@Version
	private long version;

	/**
	 * status of designation and employee is active or inactive
	 */
	@Column(columnDefinition = "TINYINT", nullable = false)
	private int status;

	@Transient
	List<AdditionalData> additionalDatas = new ArrayList<AdditionalData>();
	//
	@Transient
	Response response;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.prutech.hr.common.util.IGenericEntity#addAdditionData(com.prutech.hr.
	 * common.model.AdditionalData)
	 */
	public void addAdditionData(AdditionalData additionalData) {
		additionalDatas.add(additionalData);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.prutech.hr.common.util.IGenericEntity#setResponse(com.prutech.hr.common.
	 * model.Response)
	 */
	public void setResponse(Response response) {
		this.response = response;
	}
}
