package com.jobplanet.oauth2.model;

/**
 * 
 * Created by @author Venkat Giri 07-Mar-2019
 *
 * Constants.java
 *
 * Copyright 2018 Prutech Solutions Pvt Ltd., India. All rights reserved.
 */
public enum ConstantsEnum {

	OBJECT_NOT_FOUND("@@Object@@ with @@id@@ not found!", 4040), OBJECT("@@Object@@", 1111),
	OBJECT_OPERATION_SUCCESS("@@Operation@@ on @@Object@@ is successful", 2000), OPERATION("@@Operation@@", 9999),
	RESPONSE("response", 9999), OBJECT_OPERATION_FAILED("@@Operation@@ on @@Object@@ is failed", 5000),
	SUCCESS("success", 2000), FAILED("failed", 5000), DELETED("deleted object successfully", 2000),
	OBJECT_FOUND("@@Object@@ with @@id@@ found", 2000);

	//
	private String text;
	//
	private int code;

	/**
	 * 
	 * @param text
	 */
	private ConstantsEnum(String text, int code) {
		this.text = text;
		this.code = code;
	}

	/**
	 * 
	 * @return
	 */
	public String getText() {
		return text;
	}

	/**
	 * 
	 * @param text
	 * @return
	 */
	public ConstantsEnum fromValue(String text) {
		return valueOf(text);
	}

	/**
	 * 
	 * @return
	 */
	public int getCode() {
		return code;
	}
}
