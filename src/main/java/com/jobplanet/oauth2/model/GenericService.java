package com.jobplanet.oauth2.model;

import org.springframework.security.core.context.SecurityContextHolder;

public abstract class GenericService {
	/**
	 * 
	 * @param obj
	 * @param operation
	 * @param indicator
	 * @param args
	 * @return
	 */
	public IGenericEntity getResponse(IGenericEntity obj, String operation, ConstantsEnum indicator, Object... args) {
		switch (indicator) {
		case SUCCESS:
			return GenericUtil.rtnSucessObj(obj, operation, indicator);
		case FAILED:
		case OBJECT_NOT_FOUND:
			return GenericUtil.rtnErrorObj(obj, operation, indicator, args);
		case OBJECT_FOUND:
			return GenericUtil.objectSuccess(obj, operation, indicator, args);
		default:
			break;
		}
		return obj;
	}

	public String getLoggedInUsername() {
		if (SecurityContextHolder.getContext().getAuthentication() != null) {
			return SecurityContextHolder.getContext().getAuthentication().getName();
		} else {
			return "SpringIntegrationProcess";
		}
	}

}
