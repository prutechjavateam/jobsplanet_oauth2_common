package com.jobplanet.oauth2.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.GenericGenerator;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * 
 * Licensed to the Apache Software Foundation (ASF) under one
 * 
 * or more contributor license agreements. See the NOTICE file
 * 
 * distributed with this work for additional information
 * 
 * regarding copyright ownership. The ASF licenses this file
 * 
 * to you under the Apache License, Version 2.0 (the
 * 
 * "License"); you may not use this file except in compliance
 * 
 * with the License. You may obtain a copy of the License at
 *
 * 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * 
 * 
 * Unless required by applicable law or agreed to in writing,
 * 
 * software distributed under the License is distributed on an
 * 
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * 
 * KIND, either express or implied. See the License for the
 * 
 * specific language governing permissions and limitations
 * 
 * under the License.
 * 
 * @author Venkata Saikrishna Kodapaka
 * 
 * @createdon 07-Mar-2019
 * 
 * @Package com.prutech.oauth2.model
 * 
 * @Project oauth2-common
 *
 */
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(uniqueConstraints = { @UniqueConstraint(columnNames = { "role_name", "organization_id" }) })
@NamedStoredProcedureQuery(name = "createOrganizationRoles", procedureName = "create_org_roles", parameters = {
		@StoredProcedureParameter(mode = ParameterMode.IN, name = "org_id", type = String.class) })
public class Role extends BaseEntity implements IGenericEntity {

	//
	private static final long serialVersionUID = -7771700968001431199L;
	//
	public static final String ORG_ADMIN_NAME = "ADMIN";
	public static final String ORG_DEFAULT_NAME = "DEFAULT";
	public static final String MAKER = "MAKER";
	public static final String CHECKER = "CHECKER";
	public static final String AUDITOR = "AUDITOR";
	public static final String ORG_ADMIN_CODE = "ADMIN";

	// roleId system generated UUID (Universal Unique Identifier)
	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	@Column(name = "role_id")
	private String roleId;

	// role name
	@NotNull
	@Column(name = "role_name")
	private String roleName;

	// any remarks
	private String remarks;

	// to create role for under particular organization
	@NotNull
	@Column(name = "organization_id")
	private String organizationId;

	// list of authorities for role
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
	@JoinTable(name = "role_authorities", joinColumns = { @JoinColumn(name = "role_id") }, inverseJoinColumns = {
			@JoinColumn(name = "authority_id") })
	private List<Authority> authorities = new ArrayList<>();

}
