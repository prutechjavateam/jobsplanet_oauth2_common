package com.jobplanet.oauth2.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

import com.jobplanet.oauth2.service.LoginUserDetails;

import lombok.Data;

/**
 * 
 * 
 * Licensed to the Apache Software Foundation (ASF) under one
 * 
 * or more contributor license agreements. See the NOTICE file
 * 
 * distributed with this work for additional information
 * 
 * regarding copyright ownership. The ASF licenses this file
 * 
 * to you under the Apache License, Version 2.0 (the
 * 
 * "License"); you may not use this file except in compliance
 * 
 * with the License. You may obtain a copy of the License at
 *
 * 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * 
 * 
 * Unless required by applicable law or agreed to in writing,
 * 
 * software distributed under the License is distributed on an
 * 
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * 
 * KIND, either express or implied. See the License for the
 * 
 * specific language governing permissions and limitations
 * 
 * under the License.
 * 
 * @author Venkata Saikrishna Kodapaka
 * 
 * @createdon 07-Mar-2019
 * 
 * @Package com.prutech.oauth2.model
 * 
 * @Project oauth2-common
 * 
 * @EnhancedBy Ruchitha Vemula
 *
 *
 * 
 * 
 */
@Entity
@Table
@Data
public class User extends BaseEntity implements LoginUserDetails, Serializable, IGenericEntity {

	private static final long serialVersionUID = 8417621038737752279L;

	// userId system generated UUID (Universal Unique Identifier)
	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	@Column(name = "user_id")
	private String userId;

	// user name field
	@Column(name = "USER_NAME", unique = true)
	private String username;

	// password
	@Column(name = "PASSWORD")
	private String password;

	// Account Expired true or false
	@Column(name = "ACCOUNT_EXPIRED")
	private boolean accountExpired;

	// Account Locked true or false
	@Column(name = "ACCOUNT_LOCKED")
	private boolean accountLocked;

	// Credentials Expired true or false
	@Column(name = "CREDENTIALS_EXPIRED")
	private boolean credentialsExpired;

	// enabled true or false
	@Column(name = "ENABLED")
	private boolean enabled;

	// list of Role's to user
	@ManyToMany(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
	@JoinTable(name = "user_roles", joinColumns = { @JoinColumn(name = "user_id") }, inverseJoinColumns = {
			@JoinColumn(name = "role_id") })
	private List<Role> roles = new ArrayList<>();

	// Specifies that the property is not persistent and Collection of authorities
	@Transient
	private Collection<Authority> authorities;
	//
	/**
	 * PhoneUsername combination of userPhoneNumber|organizationid
	 */
	@Column
	private String phoneUserName;
	//
	/**
	 * Otp generated after user is loggedin with phone number
	 */
	@Column
	private String phoneUserOtp;
	//
	/**
	 * Expiry Time of Otp
	 */
	@Column
	private Date otpExpiryTime;

	//
	public boolean isAccountNonExpired() {
		return !isAccountExpired();
	}

	public boolean isAccountNonLocked() {
		return !isAccountLocked();
	}

	public boolean isCredentialsNonExpired() {
		return !isCredentialsExpired();
	}

	// To carry any kind of additional Data
	@Transient
	List<AdditionalData> additionalDataList = new ArrayList<AdditionalData>();
	//
	@Transient
	Response response;

	public void addAdditionData(AdditionalData additionalData) {
		getAdditionalDataList().add(additionalData);
	}

	public void setResponse(Response response) {
		this.response = response;
	}

	// Specifies that the property is not persistent and list of response data
	@Transient
	List<Response> responseData = new ArrayList<Response>();

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.prutech.boot.IGenericEntity#appendData(com.prutech.boot.model.
	 * ResponseData)
	 * 
	 */
	public void appendData(Response data) {
		responseData.add(data);
	}

}
