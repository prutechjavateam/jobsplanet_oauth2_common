package com.jobplanet.oauth2.model;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdditionalData implements Serializable {

	private static final long serialVersionUID = 3455794464392622433L;
	//
	String key;
	//
	Object value;
}
