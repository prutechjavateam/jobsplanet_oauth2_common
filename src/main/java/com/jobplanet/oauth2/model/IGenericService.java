package com.jobplanet.oauth2.model;

/**
 * 
 * Created by @author Venkat Giri 07-Mar-2019
 *
 * IGenericService.java
 *
 * Copyright 2018 Prutech Solutions Pvt Ltd., India. All rights reserved.
 */
public interface IGenericService<T, ID> {
	/**
	 * 
	 * @param obj
	 * @param operation
	 * @param indicator
	 * @param args
	 * @return
	 */
	public IGenericEntity getResponse(IGenericEntity obj, String operation, ConstantsEnum indicator, Object... args);

}
