package com.jobplanet.oauth2.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 * 
 * Licensed to the Apache Software Foundation (ASF) under one
 * 
 * or more contributor license agreements. See the NOTICE file
 * 
 * distributed with this work for additional information
 * 
 * regarding copyright ownership. The ASF licenses this file
 * 
 * to you under the Apache License, Version 2.0 (the
 * 
 * "License"); you may not use this file except in compliance
 * 
 * with the License. You may obtain a copy of the License at
 *
 * 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * 
 * 
 * Unless required by applicable law or agreed to in writing,
 * 
 * software distributed under the License is distributed on an
 * 
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * 
 * KIND, either express or implied. See the License for the
 * 
 * specific language governing permissions and limitations
 * 
 * under the License.
 * 
 * @author Venkata Saikrishna Kodapaka
 * 
 * @createdon 07-Mar-2019
 * 
 * @Package com.prutech.oauth2.annotation
 * 
 * @Project oauth2-common
 *
 * 
 * 
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface SystemAuthority {
	String name();

	boolean isDefault() default false;

	boolean isAdmin() default false;
}
