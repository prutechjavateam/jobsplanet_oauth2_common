package com.jobplanet.oauth2.service;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.jobplanet.oauth2.constants.ClassConstants;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * Licensed to the JobPlanet (JP) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership. The ASF licenses this file to you
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 * @author Ruchitha Vemula
 * @createdon 22-Jan-2020
 * @Package com.prutech.oauth2.service
 * @Project oauth2-common
 *
 */
@Component
@Slf4j
public class SocialServiceBeansProvider implements ApplicationContextAware {

	//
	static ApplicationContext ctx;
	//
	static String USER_REPO_BEAN = "userRepository";
	//
	static String SERVER_PORT_PROPERTY = "server.port";
	//
	static String SOCIAL_ACCOUNT_REPOSITORY = "socialAccountsRepository";

	/*
	 * 
	 */
	@Override
	public void setApplicationContext(ApplicationContext ctx) throws BeansException {
		this.ctx = ctx;

	}

	/**
	 * 
	 * @param beanId
	 * @return
	 */
	public static Object getService(String beanId) {
		String method = new Throwable().getStackTrace()[0].getMethodName();
		log.debug("{} {}", beanId, ctx.getBean(beanId), ClassConstants.DoubleColon, method);
		return ctx.getBean(beanId);
	}

	/**
	 * 
	 * @return
	 */
	public static String getServerPort() {
		String method = new Throwable().getStackTrace()[0].getMethodName();
		log.debug("{} {}", ctx.getEnvironment().getProperty(SERVER_PORT_PROPERTY), ClassConstants.DoubleColon, method);
		return ctx.getEnvironment().getProperty(SERVER_PORT_PROPERTY);
	}

}
