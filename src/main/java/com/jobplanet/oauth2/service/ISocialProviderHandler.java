package com.jobplanet.oauth2.service;

import java.util.List;
import java.util.Map;
import org.springframework.security.oauth2.common.OAuth2AccessToken;

import com.jobplanet.oauth2.exceptions.SocialLoginException;
import com.jobplanet.oauth2.model.ProviderSetup;
import com.jobplanet.oauth2.model.SocialLoginRequestBodyDTO;

/**
 * 
 * Licensed to the JobPlanet (JP) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership. The ASF licenses this file to you
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 * @author Ruchitha Vemula
 * @createdon 22-Jan-2020
 * @Package com.prutech.oauth2.service
 * @Project oauth2-common
 *
 */
public interface ISocialProviderHandler {
	//
	static final String PROVIDER_NOT_FOUND = "Provider not found for this organizationId";
	//
	static final String SETTINGS = "SETTING_";

	static final String AUTHENTICATION_SERVICE = "authentication-service";

	static final String STATIC_VALUES = "STATIC_";
	//
	static final String SEP = "|";

	/**
	 * 
	 * @return
	 */
	public String getProvider();

	/**
	 * 
	 * @return
	 * @throws SecurityException
	 * @throws NoSuchFieldException
	 */
	public Map<String, Object> getProviderAttributes()
			throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException;

	/**
	 * 
	 * @param organizationId
	 * @param configParams
	 * @return
	 */
	public List<ProviderSetup> configureProvider(String organizationId, Map<String, String> configParams);

	/**
	 * 
	 * @param socialToken
	 * @param organizationId
	 * @return
	 * @throws SocialLoginException
	 */
	public OAuth2AccessToken getPlatformToken(SocialLoginRequestBodyDTO requestBody) throws SocialLoginException;

}
