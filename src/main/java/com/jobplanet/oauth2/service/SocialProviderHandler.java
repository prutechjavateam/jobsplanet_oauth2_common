
package com.jobplanet.oauth2.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.security.oauth2.client.token.grant.password.ResourceOwnerPasswordResourceDetails;
import org.springframework.security.oauth2.common.OAuth2AccessToken;

import com.jobplanet.oauth2.constants.ClassConstants;
import com.jobplanet.oauth2.enums.ProviderStatusEnum;
import com.jobplanet.oauth2.exceptions.SocialLoginException;
import com.jobplanet.oauth2.model.ProviderSetup;
import com.jobplanet.oauth2.model.SocialAccounts;
import com.jobplanet.oauth2.model.SocialLoginRequestBodyDTO;
import com.jobplanet.oauth2.model.SocialUser;
import com.jobplanet.oauth2.repository.SocialAccountsRepository;
import com.jobplanet.oauth2.repository.UserRepository;
import com.jobplanet.oauth2.templates.Templates;
import com.jobplanet.oauth2.utils.CommunicationUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * Licensed to the JobPlanet (JP) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership. The ASF licenses this file to you
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 * @author Ruchitha Vemula
 * @createdon 22-Jan-2020
 * @Package com.prutech.oauth2.service
 * @Project oauth2-common
 *
 */
@Slf4j
public abstract class SocialProviderHandler implements ISocialProviderHandler {

	/**
	* 
	*/

	String provider;

	/**
	 * 
	 * @param provider
	 */

	public SocialProviderHandler(String provider) {
		this.provider = provider;
	}

	/**
	 * This Method Is To Get Required Static Values Of Particular Provider
	 * 
	 * @throws SecurityException
	 * @throws NoSuchFieldException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * 
	 */
	public Map<String, Object> getProviderAttributes()
			throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		String method = new Throwable().getStackTrace()[0].getMethodName();
		Map<String, Object> socialStaticNamesWithValues = new HashMap<>();
		Arrays.stream(getClass().getDeclaredFields()).filter(f -> f.getName().startsWith(STATIC_VALUES)).forEach(f -> {
			try {
				socialStaticNamesWithValues.put(f.getName().replace(STATIC_VALUES, ""), f.get(this));
				log.debug("{}", socialStaticNamesWithValues, ClassConstants.DoubleColon, method);
			} catch (IllegalArgumentException | IllegalAccessException e) {
				e.printStackTrace();
			}

		});
		return socialStaticNamesWithValues;
	}

	/**
	* 
	*/

	@Override
	public String getProvider() {
		String method = new Throwable().getStackTrace()[0].getMethodName();
		log.debug("{}", provider, ClassConstants.DoubleColon, method);
		return provider;
	}

	/**
	* 
	*/
	@Override
	public List<ProviderSetup> configureProvider(String organizationId, Map<String, String> configParams) {
		String method = new Throwable().getStackTrace()[0].getMethodName();
		List<ProviderSetup> socialParams = new ArrayList<>();
		configParams.keySet().forEach(s -> {
			ProviderSetup setup = new ProviderSetup();
			setup.setOrganizationId(organizationId);
			setup.setKey(s);
			setup.setValue(configParams.get(s));
			setup.setProvider(this.getProvider());
			setup.setStatus(ProviderStatusEnum.ACTIVE);
			setup.setGraphApiUrl(STATIC_VALUES);
			socialParams.add(setup);
			log.debug("{} {}", setup, socialParams, ClassConstants.DoubleColon, method);
		});
		return ((SocialProviderService) SocialServiceBeansProvider.getService(AUTHENTICATION_SERVICE))
				.saveProviderSetUp(socialParams);
	}

	/**
	 * This Method Is To Get Provider Configurations For Given Organization Id
	 * 
	 * @param orgniztionId
	 * @return
	 */

	protected Map<String, String> getConfig(String orgniztionId) {
		String method = new Throwable().getStackTrace()[0].getMethodName();
		Map<String, String> params = new HashMap<>();
		((SocialProviderService) SocialServiceBeansProvider.getService(AUTHENTICATION_SERVICE))
				.getConfigParams(orgniztionId, this.getProvider()).forEach(s -> {
					params.put(s.getKey(), s.getValue());
				});
		log.debug("{} {}", orgniztionId, params, ClassConstants.DoubleColon, method);
		return params;
	}

	/**
	 * 
	 * @param facebookUser
	 * @param requestBodyDTO
	 * @return
	 * @throws SocialLoginException
	 */
	protected OAuth2AccessToken getOauthAccesToken(SocialUser user, SocialLoginRequestBodyDTO requestBodyDTO)
			throws SocialLoginException {
		String method = new Throwable().getStackTrace()[0].getMethodName();
		String email = user.getUserEmail();
		OAuth2AccessToken token = null;
		if (user != null && email != null) {
			String oauthUserName = new StringBuilder(email).append(SEP).append(requestBodyDTO.getOrganizationId())
					.toString();
			String providerId = new StringBuilder(user.getUserId()).append(SEP)
					.append(requestBodyDTO.getOrganizationId()).toString();
			if (!checkOauthUserUsingProviderId(providerId)) {
				if (!checkOauthUserUsingFacebookMailAndLinkAccountIfExists(oauthUserName, providerId,
						requestBodyDTO.getProvider())) {
				}
			}
			token = sendDetialsToGetOauthAccessToken(user, oauthUserName, requestBodyDTO);
			log.debug("{} {} {} {} {} {}", user, requestBodyDTO, email, oauthUserName, providerId, token,
					ClassConstants.DoubleColon, method);
		} else {
			throw new SocialLoginException(
					ClassConstants.EMAIL_FETCHING_ERROR_FROM_SOCIAL_PROVIDERS + requestBodyDTO.getProvider());
		}
		return token;

	}

	/**
	 * 
	 * @param facebookUser
	 * @param oauthUserName
	 * @param requestBodyDTO
	 * @return
	 * @throws SocialLoginException
	 */
	private OAuth2AccessToken sendDetialsToGetOauthAccessToken(SocialUser facebookUser, String oauthUserName,
			SocialLoginRequestBodyDTO requestBodyDTO) throws SocialLoginException {
		String method = new Throwable().getStackTrace()[0].getMethodName();
		ResourceOwnerPasswordResourceDetails details = null;
		OAuth2AccessToken token = null;
		try {
			details = designResourceDetails(facebookUser, oauthUserName, requestBodyDTO);
			token = CommunicationUtil.callOauthTokenApiUsingRestTemplate(details,
					CommunicationUtil.getOauthTypeUrlForOauthToken(details));
			log.debug("{} {} {} {} {}", facebookUser, requestBodyDTO, oauthUserName, details, token,
					ClassConstants.DoubleColon, method);
		} catch (Exception e) {
			throw new SocialLoginException(ClassConstants.OAUTH_CREDENTIALS_MISMATCH + e.getMessage());
		}
		return token;
	}

	/**
	 * 
	 * @param facebooUser
	 * @param oauthUserName
	 * @param requestBody
	 * @return
	 */
	private ResourceOwnerPasswordResourceDetails designResourceDetails(SocialUser facebookUser, String oauthUserName,
			SocialLoginRequestBodyDTO requestBody) {
		String method = new Throwable().getStackTrace()[0].getMethodName();
		ResourceOwnerPasswordResourceDetails resource = new ResourceOwnerPasswordResourceDetails();
		UserRepository userRepo = (UserRepository) SocialServiceBeansProvider
				.getService(SocialServiceBeansProvider.USER_REPO_BEAN);
		Optional<com.jobplanet.oauth2.model.User> oauthUser = userRepo.findByUsername(oauthUserName);
		if (oauthUser.isPresent()) {
			com.jobplanet.oauth2.model.User oauthUserExists = oauthUser.get();
			String oauthApiUrl = getOauthTokenApiUrl(SocialServiceBeansProvider.getServerPort());
			resource.setAccessTokenUri(oauthApiUrl);
			resource.setClientId(requestBody.getOauthClientId());
			resource.setClientSecret(requestBody.getOauthclientSecret());
			resource.setGrantType(ClassConstants.GRANT_TYPE);
			resource.setUsername(new StringBuilder(facebookUser.getUserId()).append(SEP)
					.append(requestBody.getOrganizationId()).toString());
			resource.setPassword(oauthUserExists.getUsername());
		}
		log.debug("{} {} {} {} {} {}", facebookUser, requestBody, oauthUserName, userRepo, oauthUser, resource,
				ClassConstants.DoubleColon, method);
		return resource;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	private boolean checkOauthUserUsingProviderId(String id) {
		String method = new Throwable().getStackTrace()[0].getMethodName();
		log.debug("{} {}", id,
				(SocialAccountsRepository) SocialServiceBeansProvider
						.getService(SocialServiceBeansProvider.SOCIAL_ACCOUNT_REPOSITORY),
				ClassConstants.DoubleColon, method);
		return (((SocialAccountsRepository) SocialServiceBeansProvider
				.getService(SocialServiceBeansProvider.SOCIAL_ACCOUNT_REPOSITORY)).findBySocialId(id) != null ? true
						: false);
	}

	/**
	 * 
	 * @param email
	 * @param organizationId
	 * @param providerId
	 * @return
	 * @throws SocialLoginException
	 */
	private boolean checkOauthUserUsingFacebookMailAndLinkAccountIfExists(String userName, String providerId,
			String provider) throws SocialLoginException {
		String method = new Throwable().getStackTrace()[0].getMethodName();
		boolean isExists = false;
		UserRepository userRepositoryOauth = (UserRepository) SocialServiceBeansProvider
				.getService(SocialServiceBeansProvider.USER_REPO_BEAN);
		Optional<com.jobplanet.oauth2.model.User> oauthUser = userRepositoryOauth.findByUsername(userName);
		if (oauthUser.isPresent()) {
			com.jobplanet.oauth2.model.User oauthUserExists = oauthUser.get();
			try {
				SocialAccountsRepository socialRepository = (SocialAccountsRepository) SocialServiceBeansProvider
						.getService(SocialServiceBeansProvider.SOCIAL_ACCOUNT_REPOSITORY);
				SocialAccounts socialAccount = new SocialAccounts();
				socialAccount.setUserId(oauthUserExists);
				socialAccount.setProvider(provider);
				socialAccount.setSocialId(providerId);
				socialRepository.save(socialAccount);
				isExists = true;
				log.debug("{} {} {} {} {} {}", userName, providerId, provider, userRepositoryOauth, oauthUser,
						socialAccount, ClassConstants.DoubleColon, method);
			} catch (Exception e) {
				throw new SocialLoginException(
						ClassConstants.ERROR_OCCURED_WHILE_LINKING_USER_WITH_SOCIAL_ACCOUNT + e.getMessage());
			}
			return isExists;
		}
		throw new SocialLoginException(ClassConstants.SYSTEM_USER_NULL + userName);
	}

	/**
	 * 
	 * @param basePort
	 * @return
	 */
	private String getOauthTokenApiUrl(String basePort) {
		String method = new Throwable().getStackTrace()[0].getMethodName();
		String baseURL = null;
		if (basePort != null && !basePort.trim().equals(ClassConstants.EMPTY)) {
			baseURL = Templates.OAUTH_TOKEN_API.replace("@@port@@", basePort);
		} else {
			baseURL = Templates.OAUTH_TOKEN_API.replace(":@@port@@", ClassConstants.EMPTY);
		}
		log.debug("{} {}", basePort, baseURL, ClassConstants.DoubleColon, method);
		return baseURL;
	}

}
