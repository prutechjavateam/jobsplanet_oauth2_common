package com.jobplanet.oauth2.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jobplanet.oauth2.constants.ClassConstants;
import com.jobplanet.oauth2.enums.ProviderStatusEnum;
import com.jobplanet.oauth2.model.ProviderSetup;
import com.jobplanet.oauth2.repository.SocialSetupRepository;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * Licensed to the JobPlanet (JP) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership. The ASF licenses this file to you
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 * @author Ruchitha Vemula
 * @createdon 22-Jan-2020
 * @Package com.prutech.oauth2.service
 * @Project oauth2-common
 *
 */
@Service(ISocialProviderHandler.AUTHENTICATION_SERVICE)
@Slf4j
public class SocialProviderService {

	@Autowired
	SocialSetupRepository socialSetupRepository;

	/**
	 * 
	 * @param smsParams
	 * @return
	 */
	public List<ProviderSetup> saveProviderSetUp(List<ProviderSetup> smsParams) {
		String method = new Throwable().getStackTrace()[0].getMethodName();
		log.debug("{}", smsParams, ClassConstants.DoubleColon, method);
		return socialSetupRepository.saveAll(smsParams);

	}

	/**
	 * 
	 * @param organizationId
	 * @param provider
	 * @return
	 */
	public List<ProviderSetup> getConfigParams(String organizationId, String provider) {
		String method = new Throwable().getStackTrace()[0].getMethodName();
		log.debug("{}", organizationId, provider, ClassConstants.DoubleColon, method);
		return socialSetupRepository.findByOrganizationIdAndStatus(organizationId, ProviderStatusEnum.ACTIVE);

	}
}
