package com.jobplanet.oauth2.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.support.SpringFactoriesLoader;
import org.springframework.stereotype.Service;

import com.jobplanet.oauth2.constants.ClassConstants;
import com.jobplanet.oauth2.enums.ProviderStatusEnum;
import com.jobplanet.oauth2.exceptions.SocialLoginException;
import com.jobplanet.oauth2.model.ProviderSetup;
import com.jobplanet.oauth2.repository.SocialSetupRepository;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * Licensed to the JobPlanet (JP) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership. The ASF licenses this file to you
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 * @author Ruchitha Vemula
 * @createdon 22-Jan-2020
 * @Package com.prutech.oauth2.service
 * @Project oauth2-common
 *
 */
@Service
@Slf4j
public class ProviderService {

	/**
	 * 
	 */
	@Autowired
	SocialSetupRepository providerSetupRepository;

	/**
	 * 
	 * @param orgnizationId
	 * @return
	 * @throws SocialLoginException
	 */
	@SuppressWarnings("unused")
	private ISocialProviderHandler getSocialProvider(String orgnizationId) throws SocialLoginException {
		String method = new Throwable().getStackTrace()[0].getMethodName();
		List<ProviderSetup> smsProviders = providerSetupRepository.findByOrganizationIdAndStatus(orgnizationId,
				ProviderStatusEnum.ACTIVE);
		ISocialProviderHandler targetProvider = null;
		if (!smsProviders.isEmpty()) {
			List<ISocialProviderHandler> messageProviders = SpringFactoriesLoader
					.loadFactories(ISocialProviderHandler.class, null);
			boolean found = false;
			ProviderSetup setup = smsProviders.get(0);
			for (ISocialProviderHandler provider : messageProviders) {
				if (provider.getProvider().equals(setup.getProvider())) {
					found = true;
					targetProvider = provider;
					break;
				}
			}
			log.debug("{} {} {} {}", orgnizationId, smsProviders, messageProviders, targetProvider,
					ClassConstants.DoubleColon, method);
			;
		} else {
			throw new SocialLoginException(orgnizationId);
		}

		return targetProvider;
	}

	/**
	 * 
	 * @param provider
	 * @return
	 * @throws SecurityException
	 * @throws NoSuchFieldException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws MessagingException
	 */
	public Map<String, Object> getProviderConfig(String provider) throws SocialLoginException, NoSuchFieldException,
			SecurityException, IllegalArgumentException, IllegalAccessException {
		String method = new Throwable().getStackTrace()[0].getMethodName();
		boolean found = false;
		List<ISocialProviderHandler> messageProviders = SpringFactoriesLoader
				.loadFactories(ISocialProviderHandler.class, null);
		for (ISocialProviderHandler spiProvider : messageProviders) {
			if (provider.equals(spiProvider.getProvider())) {
				found = true;
				return spiProvider.getProviderAttributes();
			}
			log.debug("{} {} {} {}", provider, messageProviders, spiProvider.getProvider(),
					spiProvider.getProviderAttributes(), ClassConstants.DoubleColon, method);
			;
		}
		throw new SocialLoginException(provider);
	}

	/**
	 * 
	 * @param organizationId
	 * @param configParams
	 * @param provider
	 * @return
	 * @throws MessagingException
	 */
	public List<ProviderSetup> saveSocialProviderConfig(String organizationId, Map<String, String> configParams,
			String provider) throws SocialLoginException {
		String method = new Throwable().getStackTrace()[0].getMethodName();
		List<ISocialProviderHandler> messageProviders = SpringFactoriesLoader
				.loadFactories(ISocialProviderHandler.class, null);
		boolean found = false;
		for (ISocialProviderHandler spiProvider : messageProviders) {
			if (provider.equals(spiProvider.getProvider())) {
				found = true;
				return spiProvider.configureProvider(organizationId, configParams);
			}
			log.debug("{} {} {} {}", provider, messageProviders, spiProvider.getProvider(),
					spiProvider.configureProvider(organizationId, configParams), ClassConstants.DoubleColon, method);
		}
		throw new SocialLoginException(provider);
	}

	/**
	 * 
	 * @return
	 */
	public List<String> getSMSProviders() {
		String method = new Throwable().getStackTrace()[0].getMethodName();
		List<ISocialProviderHandler> messageProviders = SpringFactoriesLoader
				.loadFactories(ISocialProviderHandler.class, null);
		List<String> providers = new ArrayList<String>();
		for (ISocialProviderHandler provider : messageProviders) {
			providers.add(provider.getProvider());
			log.debug("{} {} {}", messageProviders, provider.getProvider(), providers, ClassConstants.DoubleColon,
					method);
			;
		}
		return providers;
	}

}
