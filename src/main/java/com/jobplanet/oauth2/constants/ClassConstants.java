package com.jobplanet.oauth2.constants;

/**
 * 
 * Licensed to the JobPlanet (JP) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership. The ASF licenses this file to you
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 * @author Ruchitha Vemula
 * @createdon 22-Jan-2020
 * @Package com.prutech.oauth2.constants
 * @Project oauth2-common
 *
 */
public class ClassConstants {

	public static final String EMAIL_FETCHING_ERROR_FROM_SOCIAL_PROVIDERS = "Unable To Fetch User Email From : ";
	//
	public static final String GRANT_TYPE = "password";
	//
	public static final String ERROR_OCCURED_WHILE_LINKING_USER_WITH_SOCIAL_ACCOUNT = "Unable To Link User With Social Account  : ";
	//
	public static final String SYSTEM_USER_NULL = "No System User Exists With this UserName : ";
	//
	public static final String EMPTY = "";
	//
	public static final String OAUTH_CREDENTIALS_MISMATCH = "Unkonown Error Occured While Getting Token By The Given Credentials : ";
	//
	public static final String USER_ID = "id";
	//
	public static final String ELEMENTS = "elements";
	//
	public static final String HANDLE = "handle~";
	//
	public static final String EMAIL_ADDRESS = "emailAddress";
	//
	public static final String Parameters = "parameters";
	//
	public static final String Colon = ":";
	//
	public static final String DoubleColon = "::";
	//
	public static final String Authentication = "Authentication";
	//
	public static final String RefreshTokenException = "Exception Occured while trying to get refresh token by the given access authentication  :- ";
	//
	public static final String AccessTokenException = "Exception Occured while trying to get access token by the given access authentication  :- ";
	//
	public static final String MissingCredentials = "missing username or password";
	//
	public static final String InvalidUser = "Invalid User";
	//
	public static final String PipeSplitting = "\\|";
	//
	public static final String ColonSplitting = "\\:";
	//
	public static final String TimeConstant = "Time:";

}
